# Web Development: Alex's student repository

This repository holds html and css code that was created for my classes from CCAC. (Spring 2021)
This class mostly dealt creating basic webpages using html and css.
All assignments and tests covered here will be from this class (CIT-125).

# Assignemnt 2: Intoduction to html

This assignment is very basic. It contains a html and htmls file. I'm just using the very basics of html here.

# Assignemnt 3: Divs and Lists

This assignment contains a html file. It build off the previous assignment by adding divs, lists, and more.

# Assignemnt 4: Images, Links, Videos, and Audio

This assignment contains a css and html file. This webpage goes over my knowledge of Pokemon.
Unforunatley, the video and audio do not work anymore becasue the original source was dremoved or changed.

# Assignemnt 5: Admiral Grace Hopper

This assignment contains a css and html file. This webpage is an article about Admiral Grace Hopper.
This asignment introduced classes and css layout with float and clear.

# Assignemnt 6: Melba Roy Mouton

This assignment contains a css and html file. This webpage is an article about Melba Roy Mouton.

# Assignemnt 7: Melba Roy Mouton Part 2

This assignment contains a css and html file. This webpage is an article about Melba Roy Mouton.
This webpage is responsive unlike the previous assignment. Meaning it should wwork on many differnt sized screens.

# Assignemnt 8: Avatar The Last Airbender Fanpage

This assignment contains a css and html file. This webpage is a  fanpage dedicated to the TV series Avatar: The Last Airbender.

# Project 1: About me

This project contains a html file. This peoject is a webpage about me.

# Project 2: Store Page

This project contains a css and html file. Creates a storepage for the game, Animal Crossing New Horizons.
The background image does not work, since the original image was deleted or changed.

# Project 3: Photo Gallery

This project contains a css and html file. Creates a photo gallery of my dogs.

# Ptoject 4: Portfolio

This project contains a css and html file. Creates a portolio of all my best assignments and projects I have done for this class.
